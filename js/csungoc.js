(function (angular) {
    'use strict';
    var app = angular.module('csungoc', ['ngRoute']);

    app.directive('navContents', function () {
        return {
            templateUrl: 'navbar.html'
        };
    });

    app.controller('VersionController', ['$scope', function ($scope) {
        $scope.version = "1.1.12";
    }]);
    app.directive('versionInfo', function () {
            return {
                templateUrl: 'version.html'
            };
    });

    app.controller('ContactController', ['$scope', function ($scope) {
    }]);
    app.directive('contactInfo', function () {
            return {
                templateUrl: 'contact.html'
            };
    });

    app.controller('NavController', ['$scope', '$location', '$anchorScroll', function ($scope, $location, $anchorScroll) {
        $scope.collapseNavBar = function () {
            var navbar = document.getElementById("navbar");
            navbar.className = "collapse navbar-collapse";
            $location.hash('top');
            $anchorScroll();
        }
    }]);

    app.controller('CalendarController', ['$scope', function ($scope) {
            $scope.smallGroupCalendar = false;
            $scope.bibleStudyCalendar = true;
    }]);
    app.directive('calendar', function () {
            return {
                templateUrl: 'calendar.html'
            };
        });

    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'home.html'
                })
                .when('/about', {
                    templateUrl: 'about.html'
                })
                .when('/media', {
                    templateUrl: 'media.html'
                })
                .when('/smallgroups', {
                    templateUrl: 'smallgroups.html'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }]);
})(window.angular);