# README #

To get access or change settings on this Project, be sure to contact the repo owner & admin before applying changes.

### What is this repository for? ###

* Remake website into mobile friendly site at the request of Paul Twiss
* Development: PROD - released 05/02/2018
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Have either the IDE or SourceTree pull down the project. No other set up requirements.
* note: this was started with IntelliJ IDE project - static web

### Contribution guidelines ###

* Branch from master when working on new features
* Merge to master only when approved

### Who do I talk to? ###

* Repo owner and admin: Daniel Ing
* Current lead: Paul Pitts III