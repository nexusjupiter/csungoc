<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title> Grace on Campus </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description"
          content="We are a ministry of Grace Community Church in Sun Valley, California. As an extension of the church, we focus on Bible teaching, leadership training, evangelism, and discipleship."/>
    <meta name="keywords"
          content="Grace on Campus, csun, csun-goc, goc, student ministry, grace community church,  csun goc"/>
    <meta name="author" content="Joon-Sub Chung"/>
    <meta name="copyright" content="Copyright&copy; 2009 Joon-Sub Chung, All Rights Reserved"/>
    <link href="../css/stylesheet.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="ajax.js"></script>

    <!--Font Awesome CDN-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>

<?php
if ($_SESSION['status'] == 'authorized') {
    require_once 'includes/constants.php';
    mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());
    mysql_select_db(DB_NAME) or die(mysql_error());

    $result = mysql_query("SELECT * FROM users WHERE username='" . $_SESSION['username'] . "'")
    or die(mysql_error());

// store the record of the "example" table into $row
    $row = mysql_fetch_array($result);

// Print out the contents of the entry 
    if ($row['isNew'] == 0) {
        echo "<div id='noticeTop'>";
        echo "<div class='inner'> ";

        echo "</div> ";
        echo "</div>";
    }
}
?>

<div id="wrapper">
    <div class="inner clearfix">
        <div id="media">
            <div class="media_link">
                <?php
                include('paginator.class.php');
                include('connect.php');

                $query = "SELECT * FROM sermons ORDER BY date DESC $pages->limit ";
                $numresults = mysql_query($query);
                $numrows = mysql_num_rows($numresults);

                $pages = new Paginator;
                $pages->items_total = mysql_num_rows($numresults);
                $pages->mid_range = 5;
                $pages->paginate();
                echo $pages->display_pages();
                echo "Page $pages->current_page of $pages->num_pages";
                ?>
            </div>
            <?php
            $query = "SELECT * FROM sermons ORDER BY date DESC $pages->limit ";
            $result = mysql_query($query) or die("Couldn't execute query");
            echo "<ul>";
            while ($row = mysql_fetch_array($result)) {
                $title = $row["title"];
                $preacher = $row["preacher"];
                $passage = $row["passage"];

                $year = substr($row['date'], 0, 4);
                $month = substr($row['date'], 5, 7);
                $day = substr($row['date'], 8, 10);

                echo "<li>";
                echo "<div class='left_down fa fa-download'></div>";
                echo "<div class='right_info'>";
                echo date('D, M d, Y', mktime(0, 0, 0, $month, $day, $year)) . "<br />";
                echo "<p>";
                echo "<span class='sermon_title'>";
                if ($row["URL"] != "none") {
                    $url = $row["URL"];
                    echo "<a target='_blank' href=\"/archive/sermons/$url\">$title</a></span><br />";
                } else
                    echo "$title</span><br />";
                echo $passage . " | " . $preacher;
                echo "</p>";
                echo "</div>";
                echo "</li>";
            }
            echo "</ul>";
            ?>
        </div>
        <div class="media_link">
            <?php
            echo $pages->display_pages();
            ?>
        </div>
    </div>
</div>

</body>
</html>